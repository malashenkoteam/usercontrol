package com.malashenko.usercontrol.controller;

import com.malashenko.usercontrol.model.User;
import com.malashenko.usercontrol.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

@RestController
@RequestMapping(value = "/users")
public class UserController {

    @Autowired
    UserService userService;

    // Получение пользователей
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView getAll(){
        ModelAndView mov = new ModelAndView("main");
        mov.addObject("users", userService.getAllUsers());
        return mov;
    }

    // Метод возвращает jsp для добавления
    @RequestMapping(value = "/addUser", method = RequestMethod.GET)
    public ModelAndView addView(){
        ModelAndView mov = new ModelAndView("add");
        mov.addObject("user",  new User());
        return mov;
    }

    // Метод возвращает jsp с данными пользователя для изменения
    @RequestMapping(value = "/{userId}", method = RequestMethod.GET)
    public ModelAndView getUser(@PathVariable(value = "userId") Integer userId){
        ModelAndView mov = new ModelAndView("update");
        mov.addObject("user", userService.getUserById(userId));
        return mov;
    }

    // Добавление нового пользователя
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ModelAndView addNewUser(@ModelAttribute("user") User user, BindingResult result){
        userService.addUser(user);
        return new ModelAndView(new RedirectView("/users"));
    }

    // Удаление пользователя
    @RequestMapping(value = "/delete/{userId}", method = RequestMethod.GET)
    public ModelAndView deleteUser(@PathVariable("userId") Integer userId){
        userService.deleteUser(userId);
        return new ModelAndView(new RedirectView("/users"));
    }

    // Изменение пользователя
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public ModelAndView updateUser(@ModelAttribute("user") User user){
        userService.updateUser(user);
        return new ModelAndView(new RedirectView("/users"));
    }
}
