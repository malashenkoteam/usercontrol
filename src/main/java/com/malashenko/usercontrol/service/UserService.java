package com.malashenko.usercontrol.service;

import com.malashenko.usercontrol.model.User;
import com.malashenko.usercontrol.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service("userService")
public class UserService {

    @Autowired
    UserRepository userRepository;

    // Получение пользователей
    @Transactional
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }
    // Добавление нового пользователя
    @Transactional
    public void addUser(User user){
        userRepository.save(user);
    }

    // Удаление пользователя
    @Transactional
    public void deleteUser(Integer userId){
        userRepository.delete(userId);
    }

    // Получение по id
    @Transactional
    public User getUserById(Integer id){
        return userRepository.findOne(id);
    }

    // Изменение пользователя
    @Transactional
    public void updateUser(User user){
        userRepository.save(user);
    }

}

