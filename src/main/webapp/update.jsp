<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isErrorPage="true" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Изменение пользователей</title>
    <style>
        <%@include file='resources/css/bootstrap.min.css' %>
        <%@include file='resources/css/main.css' %>
        <%@include file='resources/css/jquery-ui.css' %>
        <%@include file='resources/css/jquery-ui.structure.css' %>
        <%@include file='resources/css/jquery-ui.theme.css' %>
    </style>
</head>
<body>
<form:form method="post" action="/users/update" commandName="user">
    <div style="text-align: center;">
        <h2 align="center">Изменение пользователей:</h2>
        <br>
        <table width="25%" border="0" cellpadding="1" align="center">
            <tr>
                <td><form:hidden  path="id" /></td>
            </tr>
            <tr>
                <td><form:label path="name">
                    <spring:message text="ФИО"/>
                </form:label></td>
                <td><form:input path="name" required="required" value='${user.name}'/></td>
            </tr>
            <tr>
                <td><form:label path="username">
                    <spring:message text="Логин"/>
                </form:label></td>
                <td><form:input path="username" required="required" value='${user.username}'/></td>
            </tr>
            <tr>
                <td><form:label path="password">
                    <spring:message text="Пароль"/>
                </form:label></td>
                <td><form:input path="password" required="required" value='${user.password}'/></td>
            </tr>
            <tr>
                <td><form:label path="info">
                    <spring:message text="О себе"/>
                </form:label></td>
                <td><form:input path="info" required="required" value='${user.info}'/></td>
            </tr>
            <tr>
                <td><form:label path="address">
                    <spring:message text="Адрес"/>
                </form:label></td>
                <td><form:input path="address" required="required" value='${user.address}'/></td>
            </tr>
        </table>
    <br>
    <input class="btn btn-primary" type="submit" value="<spring:message text="Изменить"/>"/>
    <a class="btn btn-primary" type="submit" href="/users">Отмена</a>
    </div>
</form:form>
</body>
</html>
