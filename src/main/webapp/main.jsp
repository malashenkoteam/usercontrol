<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Панель пользователей</title>

    <style type="text/css">
        <%@include file='resources/css/bootstrap.min.css' %>
        <%@include file='resources/css/main.css' %>
        <%@include file='resources/css/jquery-ui.css' %>
        <%@include file='resources/css/jquery-ui.structure.css' %>
        <%@include file='resources/css/jquery-ui.theme.css' %>
    </style>
</head>
<body>
    <div class="tableUsers" style="text-align: center;">
        <h2 align="center">Пользователи:</h2>
        <br>
        <table class="table table-condensed" border="0" cellpadding="1" align="center">
            <tr>
                <td><input id="add" name="add" type="button"  value="+"
                               onclick="javascript:window.location='/users/addUser'"></th></td>
                <td>ФИО</td>
                <td>Логин</td>
                <td>Пароль</td>
                <td>О себе</td>
                <td>Адрес</td>
            </tr>
            <c:forEach items="${users}" var="user">
                <tr>
                    <td>
                        <input id="del" name="del" type="button"  value="del"
                                    onclick="javascript:window.location='/users/delete/'+ '${user.id}'">
                        <input id="edit" name="edit" type="button"  value="upd"
                               onclick="javascript:window.location='/users/'+ '${user.id}'">
                    </td>
                    <td>${user.name}</td>
                    <td>${user.username}</td>
                    <td>${user.password}</td>
                    <td>${user.info}</td>
                    <td>${user.address}</td>
                </tr>
            </c:forEach>
        </table>
    </div>

</body>
</html>
